import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Map from "./screens/Map";
import CameraScreen from "./screens/CameraScreen";
import SensorScreen from "./screens/SensorScreen";
const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Map" component={Map} />
      <Stack.Screen name="Camera" component={CameraScreen} />
      <Stack.Screen name="Sensors" component={SensorScreen} />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}

import { StatusBar } from "expo-status-bar";
import { Button, StyleSheet, Text, View, ScrollView } from "react-native";
import * as Location from "expo-location";
import { useState, useEffect } from "react";
import { LocationObject } from "expo-location";
import axios, { AxiosResponse } from "axios";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import * as Haptics from "expo-haptics";
import MapView from "react-native-maps";
import { SafeAreaView } from "react-native-safe-area-context";

export default function Map({ navigation }: NativeStackScreenProps<any>) {
  const [location, setLocation] = useState<LocationObject>(undefined);
  const [errorMsg, setErrorMsg] = useState<string>("");
  const [locationName, setLocationName] = useState<string>("");
  //Initial
  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }
      Location.watchPositionAsync({ distanceInterval: 1 }, getLocation);
    })();
  }, []);

  let text = "Waiting..";
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }

  async function getLocation() {
    let location = await Location.getCurrentPositionAsync();
    setLocation(location);
    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
    getLocationName(location.coords.longitude, location.coords.latitude);
  }
  async function getLocationName(lon: number, lat: number): Promise<void> {
    const apiKey = "5b3ce3597851110001cf6248701dc564633d4c65a552aa78e83b6f43";

    if (!lon || !lat) {
      Haptics.notificationAsync(Haptics.NotificationFeedbackType.Error);
      console.log("Unable to retrieve location");
    } else {
      const apiUrl = `https://api.openrouteservice.org/geocode/reverse?api_key=${apiKey}&point.lon=${lon}&point.lat=${lat}`;
      console.log(apiUrl);
      await axios
        .get(apiUrl)
        .catch((x) => {
          console.log(x);
        })
        .then((x: AxiosResponse) => {
          setLocationName(x.data?.features[0]?.properties?.label);
        });
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <MapView
        style={styles.map}
        showsUserLocation={true}
        region={{
          latitude: location?.coords?.latitude || 0,
          longitude: location?.coords?.longitude || 0,
          latitudeDelta: 0.00922,
          longitudeDelta: 0.00421,
        }}
        initialRegion={{
          latitude: location?.coords?.longitude || 0,
          longitude: location?.coords?.longitude || 0,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      />
      <ScrollView style={styles.scrollView}>
        <View style={styles.view}>
          <Text>Map shower 2000.exe</Text>
          <Text style={styles.paragraph}>Location name: {locationName}</Text>
          <Text style={styles.paragraph}>GPS info: {text}</Text>
          <StatusBar style="auto" />
          <Button
            title="Go to camera"
            onPress={() => navigation.navigate("Camera")}
          />
          <Button
            title="Go to sensors"
            onPress={() => navigation.navigate("Sensors")}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  paragraph: { maxWidth: "80%" },
  scrollView: { maxHeight: "40%" },
  view: { paddingBottom: 30 },
  map: { width: "100%", height: "60%" },
});
